FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <support@cloudron.io>

ENV PATH /usr/local/node-6.9.5/bin:$PATH

RUN mkdir -p /app/code
WORKDIR /app/code

RUN curl -L https://github.com/andris9/mailtrain/archive/v1.23.2.tar.gz | tar -xz --strip-components 1 -f -
ADD patches/fix-collation.patch /app/code/fix-collation.patch
RUN patch -p1 -d /app/code < /app/code/fix-collation.patch
RUN npm install --production && npm install passport-ldapjs

RUN ln -s /run/production.toml /app/code/config/production.toml

ADD cloudron-setup.js production.toml.template start.sh /app/code/
ADD index.hbs layout.hbs /app/code/views/

CMD [ "/app/code/start.sh" ]
