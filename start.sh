#!/bin/bash

set -eu

export NODE_ENV=production

# Copy the default configuration template to a user-modifiable place
if [ ! -f /app/data/production.toml.template ]; then
    cp /app/code/production.toml.template /app/data/production.toml.template
fi

sed -e "s!##MYSQL_HOST##!${MYSQL_HOST}!" \
    -e "s!##MYSQL_PORT##!${MYSQL_PORT}!" \
    -e "s!##MYSQL_USERNAME##!${MYSQL_USERNAME}!" \
    -e "s!##MYSQL_PASSWORD##!${MYSQL_PASSWORD}!" \
    -e "s!##MYSQL_DATABASE##!${MYSQL_DATABASE}!" \
    -e "s!##REDIS_HOST##!${REDIS_HOST}!" \
    -e "s!##REDIS_PORT##!${REDIS_PORT}!" \
    -e "s!##REDIS_PASSWORD##!${REDIS_PASSWORD}!" \
    -e "s!##LDAP_SERVER##!${LDAP_SERVER}!" \
    -e "s!##LDAP_PORT##!${LDAP_PORT}!" \
    -e "s!##LDAP_USERS_BASE_DN##!${LDAP_USERS_BASE_DN}!" \
    -e "s!##WEBADMIN_ORIGIN##!${WEBADMIN_ORIGIN}!" \
    /app/data/production.toml.template > /run/production.toml

cd /app/code

/usr/local/bin/gosu cloudron:cloudron node cloudron-setup.js
exec /usr/local/bin/gosu cloudron:cloudron npm start
