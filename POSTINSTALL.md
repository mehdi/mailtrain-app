Mailtrain is setup to send emails using the Cloudron SMTP server. An external server can be configured within the app.

To revert from an external email server setup to the Cloudron setup, set the SMTP Hostname to `localhost` and then restart the app.
This will inject the Cloudrons SMTP credentials automatically.
